//
//  LoginView.swift
//  ProjectIdeas
//
//  Created by Mariano Saldarriaga on 10/27/19.
//  Copyright © 2019 silverwatch. All rights reserved.
//

import SwiftUI

struct LoginView: View {
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    static let colors = Gradient(colors: [Color(UIColor(red: 0.96, green: 0.39, blue: 0.38, alpha: 1.0)), Color(UIColor(red: 0.96, green: 0.48, blue: 0.38, alpha: 1.0))])
    let conic = LinearGradient(gradient: colors, startPoint: .leading, endPoint: .trailing)
    
    var body: some View {
         NavigationView {
           ZStack{
                VStack {
                    Rectangle()
                        .fill(conic)
                        .frame(width: screenWidth,height: screenHeight*0.6, alignment: .topLeading)
                        .offset(x: 0 ,y: -60)
                    Spacer()
                }
                LogoView()
                CardViewLogin()
            }
        }
         .frame(width: screenWidth, height: screenHeight, alignment: .leading)
    }
}

struct Title : View {
    var title :String
    var body : some View {
        VStack(alignment: .center) {
            Text(verbatim: title)
                .font(.title)
                
        }
        .padding(.bottom, 15.0)
        .padding(.top, 25.0)
    }
}

struct LabelTextField : View {
    var label: String
    var placeHolder: String
    @Binding var inputText : String
 
    var body: some View {
        VStack(alignment: .leading) {
            Text(verbatim: label)
                .font(.headline)
            TextField("", text: $inputText)
                .textContentType(.emailAddress)
                .padding(.all)
                .background(Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0))
            .clipShape(RoundedRectangle(cornerRadius: 20))
            .shadow(radius: 1)
            }
        .padding(.horizontal, 25)
    }
}

struct SecureTextField : View {
    var label: String
    var placeHolder: String
    @Binding var inputText : String
 
    var body: some View {
        VStack(alignment: .leading) {
            Text(verbatim: label)
                .font(.headline)
            SecureField("", text: $inputText)
                .padding(.all)
                .background(Color(red: 239.0/255.0, green: 243.0/255.0, blue: 244.0/255.0, opacity: 1.0))
            .clipShape(RoundedRectangle(cornerRadius: 20))
            .shadow(radius: 1)
            }
            .padding(.horizontal, 25)
        
    }
}


struct CardViewLogin: View {
 @State private var email : String = ""
 @State private var password : String = ""
let colorBorder = Color(UIColor(red: 0.96, green: 0.48, blue: 0.38, alpha: 1.0))
    @State var tag : Int? = nil
        @ObservedObject var loginVM = LoginViewModel()
    var body: some View {
        
            VStack {
                 Title(title: "Iniciar sesion")
                LabelTextField(label: "Correo", placeHolder: "", inputText: $email)
                SecureTextField(label: "Contraseña", placeHolder: "", inputText: $password)
                NavigationLink(destination: HomeView(), tag: 1, selection: $tag) {
                    Button(action: {
                        
                        print("email " + self.email + "password " + self.password)
                       
                        self.loginVM.fetchLogin(email: self.email, password: self.password) { value in
                            if (value != nil){
                                self.tag = value
                            }
                            
                        }
                       
                    }) {
                        HStack {
                            Spacer()
                            Text("Iniciar")
                                .font(.headline)
                                .foregroundColor(colorBorder)
                            Spacer()
                        }
                        .background(Color.white)
                        .padding()
                        .overlay(RoundedRectangle(cornerRadius: 40)
                        .stroke(colorBorder, lineWidth: 3))
                    }
                }
                .padding(.top, 15)
                .padding(.vertical, 15.0)
                .padding(.horizontal, 20)
                 RegisterRoundedButton()
            }
            .background(Color.white)
            .cornerRadius(30)
            .padding(10)
            .shadow(radius: 10)
        
    }
    
    
}



struct LoginRoundedButton : View {
    
    @ObservedObject var loginVM = LoginViewModel()
    
    @State var email : String
    @State var password : String
    @State var tag : Int? = nil

    let colorBorder = Color(UIColor(red: 0.96, green: 0.48, blue: 0.38, alpha: 1.0))
    var body: some View {
        NavigationLink(destination: HomeView(), tag: 1, selection: $tag) {
                Button(action: {
                    
                    print("email " + self.email + "password " + self.password)
                    
                 
                }) {
                    HStack {
                        Spacer()
                        Text("Iniciar")
                            .font(.headline)
                            .foregroundColor(colorBorder)
                        Spacer()
                    }
                    .background(Color.white)
                    .padding()
                    .overlay(RoundedRectangle(cornerRadius: 40)
                    .stroke(colorBorder, lineWidth: 3))
                }
            }
            .padding(.top, 15)
            .padding(.vertical, 15.0)
            .padding(.horizontal, 20)
    }
}

struct RegisterRoundedButton : View {
    @State var tag: Int? = nil
    let colorBorder = Color(UIColor(red: 0.96, green: 0.48, blue: 0.38, alpha: 1.0))
    var body: some View {
    
        NavigationLink(destination: RegisterView(), tag: 1, selection: $tag) {
            Button(action : {
                self.tag = 1
            }) {
                HStack {
                      Spacer()
                      Text("Registrarse")
                          .font(.headline)
                          .foregroundColor(colorBorder)
                      Spacer()
                  }
                .background(Color.white)
                .padding()
                .overlay(RoundedRectangle(cornerRadius: 40)
                .stroke(colorBorder, lineWidth: 3))
            }
          }
          .padding(.vertical, 15.0)
          .padding(.horizontal, 20)
          .padding(.bottom, 20)
    }
}


struct LogoView : View {
    var body : some View {
        ZStack {
            VStack (alignment: .leading) {
                Image("light_bulb_logo")
                .resizable()
                .frame(width: 170, height: 170)
                    .position(x: 200, y:70 )
            }
            Text("Proyecto Ideas")
                .fontWeight(.bold)
                .font(.custom("Arial", size: 25))
            .position(x: 200, y:170 )
        }
    }
}

func LoginAuthenticate(email: String , password : String, completion: @escaping  (Data?, URLResponse?, Error?) -> ()){
    let url = URL(string: "https://aplicacionesmovilesbe.herokuapp.com/login")!
    let json : [String:Any] =
    [
        "email": email ,
        "password": password
    ]
    let jsonData = try? JSONSerialization.data(withJSONObject: json, options: [])
    var request = URLRequest(url:url)
    request.httpMethod = "POST"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpBody = jsonData
    let task = URLSession.shared.dataTask(with: request){ data , response, error in
        completion(data, response,error)
    }
    task.resume()
}


struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}


    
