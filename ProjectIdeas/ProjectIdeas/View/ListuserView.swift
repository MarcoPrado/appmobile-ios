//
//  UserView.swift
//  ProjectIdeas
//
//  Created by Mariano Saldarriaga on 11/7/19.
//  Copyright © 2019 silverwatch. All rights reserved.
//

import SwiftUI

struct UserView: View {
    
    init() {
         UITableView.appearance().separatorStyle = .none
     }
    
    var body: some View {
         NavigationView{
           List{
               CardView(image: "wp10", category: "Usuariossss", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
               CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
               CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
               CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
               CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
               CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
               CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
           }.navigationBarHidden(true)
           .navigationBarBackButtonHidden(true)
           .statusBar(hidden: true)
       }
    }
}

struct UserView_Previews: PreviewProvider {
    static var previews: some View {
        UserView()
    }
}
