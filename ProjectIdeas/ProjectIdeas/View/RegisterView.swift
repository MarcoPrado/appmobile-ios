//
//  RegisterView.swift
//  ProjectIdeas
//
//  Created by Mariano Saldarriaga on 11/7/19.
//  Copyright © 2019 silverwatch. All rights reserved.
//

import SwiftUI

func Register(fullName : String, lastName: String ,email: String , password : String, completion: @escaping  (Data?, URLResponse?, Error?) -> ()){
    let url = URL(string: "https://aplicacionesmovilesbe.herokuapp.com/users")!
    let json : [String:Any] =
    [
        "name" : fullName,
        "lastName" : lastName,
        "email" : email,
        "password": password
    ]
    let jsonData = try? JSONSerialization.data(withJSONObject: json, options: [])
    var request = URLRequest(url:url)
    request.httpMethod = "POST"
    request.setValue("application/json", forHTTPHeaderField: "Content-Type")
    request.httpBody = jsonData
    
    let task = URLSession.shared.dataTask(with: request){ data , response, error in
        completion(data, response,error)
    }
    task.resume()
}

struct RegisterView: View {
    @State private var fullName: String = ""
    @State private var lastName: String = ""
    @State private var email: String = ""
    @State private var password: String = ""
    @State private var repeatPassword: String = ""
    
    let screenWidth = UIScreen.main.bounds.width
    let screenHeight = UIScreen.main.bounds.height
    static let colors = Gradient(colors: [Color(UIColor(red: 0.96, green: 0.39, blue: 0.38, alpha: 1.0)), Color(UIColor(red: 0.96, green: 0.48, blue: 0.38, alpha: 1.0))])
    let conic = LinearGradient(gradient: colors, startPoint: .leading, endPoint: .trailing)
    
    var body: some View {
          NavigationView{
            ZStack {
                VStack {
                    Rectangle()
                        .fill(conic)
                        .frame(width: screenWidth,height: screenHeight*0.6, alignment: .topLeading)
                        .offset(x: 0 ,y: -60)
                    Spacer()
                }
                VStack {
                           Title(title: "Registrarse")
                    LabelTextField(label: "Nombres", placeHolder: "", inputText: $fullName)
                    LabelTextField(label: "Apellidos", placeHolder: "", inputText: $lastName)
                       LabelTextField(label: "Correo", placeHolder: "", inputText: $email)
                    SecureTextField(label: "Contraseña", placeHolder: "", inputText: $password)
                            SecureTextField(label: "Repetir contraseña", placeHolder: "", inputText: $repeatPassword)
                           RegisterrRoundedButton()
                    CancelRoundedButton()
                           
                       }
                .background(Color.white)
                   .cornerRadius(30)
                   .padding()
                   .shadow(radius: 10)
                .position(x:207, y:400)
            }
       
        }.frame(width: screenWidth, height: screenHeight, alignment: .leading)
        .navigationBarBackButtonHidden(true)
    }
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
        
    }
}

struct RegisterrRoundedButton : View {
     let colorBorder = Color(UIColor(red: 0.96, green: 0.48, blue: 0.38, alpha: 1.0))
       var body: some View {
               NavigationLink(destination: LoginView()) {
                   HStack {
                       Spacer()
                       Text("Registrarse")
                           .font(.headline)
                           .foregroundColor(colorBorder)
                       Spacer()
                   }
                   .background(Color.white)
                   .padding()
                   .overlay(RoundedRectangle(cornerRadius: 40)
                   .stroke(colorBorder, lineWidth: 3))
               }
               .padding(.vertical, 25.0)
               .padding(.horizontal, 20)
       }
}

struct CancelRoundedButton : View {
    let colorBorder = Color(UIColor(red: 0.96, green: 0.48, blue: 0.38, alpha: 1.0))
    var body: some View {
            NavigationLink(destination: LoginView()) {
                HStack {
                    Spacer()
                    Text("Cancelar")
                        .font(.headline)
                        .foregroundColor(colorBorder)
                    Spacer()
                }
                .background(Color.white)
                .padding()
                .overlay(RoundedRectangle(cornerRadius: 40)
                .stroke(colorBorder, lineWidth: 3))
            }
            .padding(.horizontal, 20)
            .padding(.bottom,35)
    }
}
