//
//  ProjectsView.swift
//  ProjectIdeas
//
//  Created by Mariano Saldarriaga on 11/7/19.
//  Copyright © 2019 silverwatch. All rights reserved.
//

import SwiftUI



struct ProjectsView: View {
    init() {
        UITableView.appearance().separatorStyle = .none
    }
   
    
    var body: some View {
        NavigationView{
            List{
                CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
                CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
                CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
                CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
                CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
                CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
                CardView(image: "wp10", category: "SwiftUI", heading: "Drawing a Border with Rounded Corners", author: "Simon Ng")
            }.navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
            //.statusBar(hidden: true)
            .allowsHitTesting(true)
        }
    }
}

struct ProjectsView_Previews: PreviewProvider {
    static var previews: some View {
        ProjectsView()
    }
}


struct CardView: View {
    var image: String
    var category: String
    var heading: String
    var author: String
    
    var body: some View {
        VStack {
            Image(image)
                .resizable()
                .aspectRatio(contentMode: .fit)
         
            HStack {
                VStack(alignment: .leading) {
                    Text(category)
                        .font(.headline)
                        .foregroundColor(.secondary)
                    Text(heading)
                        .font(.title)
                        .fontWeight(.black)
                        .foregroundColor(.primary)
                        .lineLimit(3)
                    Text(author.uppercased())
                        .font(.caption)
                        .foregroundColor(.secondary)
                }
                .layoutPriority(100)
                Spacer()
            }
            .padding()
        }
        .cornerRadius(10)
        .overlay(
            RoundedRectangle(cornerRadius: 10)
                .stroke(Color(.sRGB, red: 150/255, green: 150/255, blue: 150/255, opacity: 0.1), lineWidth: 4)
        )
        .padding([.top, .horizontal])
    }
}
