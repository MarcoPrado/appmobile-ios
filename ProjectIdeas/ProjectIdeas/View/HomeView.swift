//
//  HomeView.swift
//  ProjectIdeas
//
//  Created by Mariano Saldarriaga on 11/7/19.
//  Copyright © 2019 silverwatch. All rights reserved.
//

import SwiftUI



struct HomeView: View {
    @State var activateTab:Int = 1
    
    var body: some View {
        TabView (selection: $activateTab ){
        ProjectsView().tabItem({
            Image(systemName : "house")}).tag(1)
        Text("Proyectos").tabItem({Image(systemName: "list.bullet")}).tag(2)
        UserView().tabItem({
            Image(systemName: "person.2.fill")}).tag(3)
        Text("Proyectos").tabItem({Image(systemName: "gear")}).tag(4)
        }
        
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}

