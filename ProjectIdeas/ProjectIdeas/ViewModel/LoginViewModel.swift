//
//  LoginViewModel.swift
//  ProjectIdeas
//
//  Created by Mariano Saldarriaga on 11/11/19.
//  Copyright © 2019 silverwatch. All rights reserved.
//

import Foundation

class LoginViewModel : ObservableObject {
    
    @Published var isloged : Int? = nil
    
    func fetchLogin(email: String , password : String , compilationHandler: @escaping ((Int?) -> Void)) {
        // fetch json and decode and update some information about login property        
        guard let url = URL(string: Constants.BASE_URL + "login") else { return }
        let json : [String:Any] =
        [
            "email": email ,
            "password": password
        ]
        let jsonData = try? JSONSerialization.data(withJSONObject: json, options: [])
        var request = URLRequest(url:url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        URLSession.shared.dataTask(with: request){ data , response, error in
            // make sure to check error / resp
            if(error != nil){
                self.isloged = nil
            }
            DispatchQueue.main.async {
                 let responseJSON = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? [String: Any]
                 print(responseJSON ?? "")
                 let jsonStatus = responseJSON?["status"] as! String
                 if (jsonStatus == "correctly"){
                    self.isloged = 1
                    compilationHandler(1)
                 }else{
                    self.isloged = nil
                 }
            }
        }.resume()
    }
    
}
