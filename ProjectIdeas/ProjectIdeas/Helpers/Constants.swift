//
//  Constants.swift
//  ProjectIdeas
//
//  Created by Mariano Saldarriaga on 11/7/19.
//  Copyright © 2019 silverwatch. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    static let BASE_URL : String = "https://aplicacionesmovilesbe.herokuapp.com/"
    
    struct TabBarImageName {
        static let tabBar0 = "project.fill"
        static let tabBar1 = "users.fill"
        static let tabBar2 = "myproject.fill"
        static let tabBar3 = "profile.fill"
    }
    
    struct TabBarText {
        static let tabBar0 = "Proyectos"
        static let tabBar1 = "Usuarios"
        static let tabBar2 = "Mis proyectos"
        static let tabBar3 = "...."
    }
}
