//
//  ProjectModel.swift
//  ProjectIdeas
//
//  Created by Mariano Saldarriaga on 11/11/19.
//  Copyright © 2019 silverwatch. All rights reserved.
//

import Foundation

struct ProjectModel : Identifiable, Decodable {
    let id : Int
    let title : String
    let description : String
    let points : Int
    let dateCreated : String
    let image : String
    let category : String
    let answers : [Answer]
}

struct Member : Identifiable, Decodable {
    let id : Int
    let idUser : Int
    let name : String
    let lastName : String
    let email : String
    let image : String
}

struct Answer: Decodable {
    let answer : String
    let question : String
}
