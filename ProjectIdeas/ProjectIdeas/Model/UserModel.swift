//
//  UserModel.swift
//  ProjectIdeas
//
//  Created by Mariano Saldarriaga on 11/11/19.
//  Copyright © 2019 silverwatch. All rights reserved.
//

import Foundation

struct UserModel: Identifiable, Decodable {
    let id : Int = 0
    let name : String = ""
    let lastName : String = ""
    let email : String = ""
    let phone : String = ""
    let dateCreated : String = ""
    let token : String = ""
    let birthDate : String = ""
    let image : String = ""
}
