//
//  TopicModel.swift
//  ProjectIdeas
//
//  Created by Mariano Saldarriaga on 11/11/19.
//  Copyright © 2019 silverwatch. All rights reserved.
//

import Foundation

struct TopicModel : Identifiable, Decodable {
    let id : Int
    let idProject : Int
    let title : String
    let dateCreated : String
    let comments : [Comment]
}

struct Comment : Identifiable, Decodable {
    let id : Int
    let idTopic : Int
    let comment : String
    let dateCreated : String
}
